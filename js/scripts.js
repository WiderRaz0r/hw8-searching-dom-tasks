const paragraphs = document.querySelectorAll("p");
paragraphs.forEach((p) => p.style.backgroundColor = "#FF0000")
console.log(paragraphs);

const byId = document.querySelector("#optionsList");
console.log(byId);
for (let key of byId.children) {
    console.log(key.nodeType, key.nodeName);
}

const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = '<p>This is a paragraph</p>';
console.log(testParagraph);

const nestedElems = document.querySelectorAll(".main-header > *");
nestedElems.forEach((el) => el.classList.add("nav-item"));
console.log(nestedElems);

const titleElems = document.querySelectorAll(".section-title");
console.log(titleElems);
titleElems.forEach((del) => del.classList.remove("section-title"));
console.log(titleElems);

